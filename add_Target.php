<!DOCTYPE html>
<html>
<head>
  <title></title>


  <script type="text/javascript" src="js/jquery.js"></script>
  <link rel="stylesheet" type="text/css" href="bootstrap4/css/bootstrap.min.css"/>
  <meta charset="UTF-8">

  <link rel="stylesheet" type="text/css" href="css/dashboard.css"/>

  <!-- date picker -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/gh/atatanasov/gijgo@1.7.3/dist/combined/js/gijgo.min.js" type="text/javascript"></script>
  <link href="https://cdn.jsdelivr.net/gh/atatanasov/gijgo@1.7.3/dist/combined/css/gijgo.min.css" rel="stylesheet" type="text/css" />

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>
<body>

 <?php 

 if ($_POST['submit']=="save-target") {
  require('connect.php'); 
  session_start();
  $goals = array();
  $conn = db_connect();
  $duedate=strtotime($_POST['duedate']); 
  $duedate=date("Y-m-d",$duedate);
  $sql = "INSERT INTO goal  (uid, name, price, duedate) ";
  $sql = $sql . "values (" . $_SESSION["uid"] . ", '";
  $sql = $sql .  $_POST['target-name'] . "', ";
  $sql = $sql .  $_POST['target-price'] . ", '";
  $sql = $sql . $duedate ."')";
  echo $sql;
  $result = mysqli_query($conn, $sql);
  if($result) {
    echo "yahoo";
  }
}
?>


<!-- Image and text -->
<nav class="navbar navbar-light bg-faded title-nav">
  <a class="navbar-brand" href="dashboard.html">
    <img src="images/icon2.png" width="50" height="50" class="d-inline-block align-top" alt="Home">
  </a> <span class="title-header">เพิ่มเป้าหมาย</span> 
</nav>


<form class="form-horizontal" action="" method="POST">
  <fieldset>
    <div class="group container">
      <div class="form-group row">
        <!-- target -->
        <label for="example-text-input" class="col-2 col-form-label">Target</label>
        <div class="col-10">
          <input class="form-control" type="text" name="target-name" placeholder="สิ่งของที่ต้องการ" id="example-text-input">
        </div>
      </div>
      <!-- price -->
      <div class="form-group row">
        <label for="example-search-input" class="col-2 col-form-label">Price</label>
        <div class="col-10">
          <input class="form-control" type="search" name="target-price" placeholder="ราคา" id="example-search-input">
        </div>
      </div>
      <!-- deadline -->
      <div class="form-group row">
        <label for="example-email-input" class="col-2 col-form-label">duedate</label>
        <div class="col-10">
          <input name ="duedate" id="datepicker" />
        </div>
      </div>  
      <!-- button -->
      <div class="form-group button-group">
        <button  name="submit" value="save-target" class="btn btn-outline-success save-btn">Save</button>
        <button action="action" onclick="window.history.go(-1); return false;" class="btn btn-outline-danger cancel-btn">Cancel</button>
      </div>
    </div> <!-- end container -->
  </fieldset>
</form>

<?php
echo "aaaa";


?>
<script>
  $('#datepicker').datepicker();

</script>
</body>
</html>