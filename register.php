<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title></title>

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="bootstrap4/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/dashboard.css"/>
  <link href="web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">
  <!-- JS -->
  <script type="text/javascript" src="js/jquery.js"></script>   
</head>
<body>
  <?php  require('connect.php'); ?>

  <?php 
  if ($_POST["submit"] == "Save") 
  {
    $conn = db_connect();
    $sql = "INSERT INTO members (kapook_id, username, password, email) ";
    $sql = $sql . "VALUES ('". $_POST['registercode'] . "', ";
    $sql = $sql . $_POST['name'] .  ", ";
    $sql = $sql . $_POST['password'] .  ", ";
    $sql = $sql . $_POST['email'];
    $sql = $sql . ")";
    $result = mysqli_query($conn, $sql);
    if($result) {
      // echo "<pre> Record update successfully </pre>";
      header("location: login.php");
      exit();
    } else  {
      // echo "<pre> cannot update". $sql.  "</pre>";
    } 
  }
  mysql_close($conn);
  ?>
  <div class="container">
    <form class="form-horizontal" role="form" method="POST" action="">
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <h2>Register New User</h2>
          <hr>
        </div>
      </div>
      <!-- name -->
      <div class="row">
        <div class="col-md-3 field-label-responsive">

          <label for="name">Userame</label>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
              <input type="text" name="name" class="form-control" id="name"
              placeholder="John Doe" required autofocus>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-control-feedback">
            <span class="text-danger align-middle">
            </span>
          </div>
        </div>
      </div>
      <!-- email -->
      <div class="row">
        <div class="col-md-3 field-label-responsive">
          <label for="email">E-Mail Address</label>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
              <input type="text" name="email" class="form-control" id="email"
              placeholder="you@example.com" required autofocus>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-control-feedback">
            <span class="text-danger align-middle">
              <!-- Put e-mail validation error messages here -->
            </span>
          </div>
        </div>
      </div>
      <!-- registercode -->
      <div class="row">
        <div class="col-md-3 field-label-responsive">
          <label for="email">Register Code</label>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-asterisk"></i></div>
              <input type="text" name="registercode" class="form-control" id="registercode"
              placeholder="abc123" required autofocus>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-control-feedback">
            <span class="text-danger align-middle">
              <!-- Put e-mail validation error messages here -->
            </span>
          </div>
        </div>
      </div>
      <!-- password -->
      <div class="row">
        <div class="col-md-3 field-label-responsive">
          <label for="password">Password</label>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
              <input type="password" name="password" class="form-control" id="password"
              placeholder="Password" required>
            </div>
          </div>
        </div>
      </div>
      <!-- password-confirmation -->
      <div class="row">
        <div class="col-md-3 field-label-responsive">
          <label for="password">Confirm Password</label>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <div class="input-group-addon" style="width: 2.6rem">
                <i class="fa fa-repeat"></i>
              </div>
              <input type="password" name="password-confirmation" class="form-control"
              id="password-confirm" placeholder="Password" required>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <button name="submit" value="Save" class="btn btn-success"><i class="fa fa-user-plus"></i> Register</button>
        </div>
      </div>
    </form>
  </div>


</body>
</html>