<!DOCTYPE html>
<html>
<head>
  <title>Piggy Bank</title>
  <script src="code/highcharts.js"></script>
  <script src="code/highcharts-more.js"></script>

  <script src="code/modules/solid-gauge.js"></script>
  <script src="code/modules/bullet.js"></script>
  <link rel="stylesheet" type="text/css" href="bootstrap4/css/bootstrap.min.css"/><meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="css/dashboard.css" />
</head>
<body>

  <script type="text/javascript">
    var count = 0;
  </script>

  <?php 
  require('connect.php'); 
  session_start();
  $goals = array();
  $conn = db_connect();
  mysqli_set_charset($conn,"utf8");
  $sql = "SELECT * FROM goal WHERE uid = " . $_SESSION["uid"];
  $result = mysqli_query($conn, $sql);
  if (mysqli_num_rows($result) > 0) {
    while ($row = $result->fetch_assoc()) {
      array_push($goals,$row);
    }
  }

  ?>

  <a href="add_target.php"><button type="button" class="btn btn-outline-primary add-button">Add Target</button></a>

  <H3 class="status-inprogress"> In Progress </H3>
  <div class="clear">
    <?php   
    foreach ($goals as $key => $value) {
      $sql_kapook = "SELECT * FROM kapook WHERE kapook_id = " .  $_SESSION["kid"] . " and goal_id = " . $goals[$key]['goal_id'];
      // echo "sql : " . $sql_kapook;
      $result_kapook = mysqli_query($conn, $sql_kapook);
      if (mysqli_num_rows($result_kapook) > 0) {
        $sum = 0;
        while ($row_kapook = $result_kapook->fetch_assoc()) {
          $sum = $sum + $row_kapook['coin_type'];
        }
        // echo "sum : " . $sum;
        $percent = ($sum * 100) / $goals[$key]['price'];

      }

      $OldDate = new DateTime($goals[$key]['duedate']);
      $now = new DateTime(Date('Y-m-d'));

      $interval = date_diff($now, $OldDate);
      $duration = $interval->format('%R%a days');
      
      ?>


      <div class='thing'>
        <div id= <?php echo "'" . $goals[$key]['name'] ."'"; ?> class="act-chart"></div>
        <div class="center">
          <table align="center">
            <tr>
              <td class="info">ราคา</td>
              <td class="info"></td>
              <td class="info"><?php echo $goals[$key]['price'];?> บาท</td>
            </tr>
            <tr>
              <td class="info">เก็บได้</td>
              <td class="info"><?php echo sprintf('%0.2f', $percent);?> %</td>
              <td class="info"><?php echo $sum;?> บาท</td>
            </tr>
            <tr>
              <td class="info">ขาดอีก</td>
              <td class="info"><?php echo sprintf('%0.2f', 100-$percent);?> %</td>
              <td class="info"><?php echo $goals[$key]['price']-$sum;?> บาท</td>
              <tr>
                <td class="info">เหลือเวลาอีก</td>
                <td class="info"><?php echo $interval->format('%R%a days');?></td>
                <td class="info"><?php echo $goals[$key]['duedate']; ?></td>
              </tr>
            </tr>
          </table>
        </div>
      </div>

      <script type="text/javascript">
        function renderIcons() {
    // Move icon
    if (!this.series[0].icon) {
      this.series[0].icon = this.renderer.path(['M', -8, 0, 'L', 8, 0, 'M', 0, -8, 'L', 8, 0, 0, 8])
      .attr({
        'stroke': '#303030',
        'stroke-linecap': 'round',
        'stroke-linejoin': 'round',
        'stroke-width': 2,
        'zIndex': 10
      })
    }
    this.series[0].icon.translate(
      this.chartWidth / 2 - 10,
      this.plotHeight / 2 - this.series[0].points[0].shapeArgs.innerR -
      (this.series[0].points[0].shapeArgs.r - this.series[0].points[0].shapeArgs.innerR) / 2
      ); 
  }

  var chart_name = "<?php echo $goals[$key]['name']; ?>";
  var percent = "<?php echo $percent; ?>";
  var duration = "<?php echo intval($interval->format('%R%a'))+100; ?>";
  var percent_int =  parseInt(percent);
  console.log( typeof( parseInt(percent)));


  Highcharts.chart(chart_name , {

    chart: {
      type: 'solidgauge',
      height: '110%',
      events: {
        render: renderIcons
      }
    },

    title: {
      text: "<?php echo($goals[$key]['name']); ?>",
      style: {
        fontSize: '24px'
      }
    },

    tooltip: {
      borderWidth: 0,
      backgroundColor: 'none',
      shadow: false,
      style: {
        fontSize: '16px'
      },
      pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}%</span>',
      positioner: function (labelWidth) {
        return {
          x: (this.chart.chartWidth - labelWidth) / 2,
          y: (this.chart.plotHeight / 2) + 15
        };
      }
    },

    pane: {
      startAngle: 0,
      endAngle: 360,
        background: [{ // Track for Move
          outerRadius: '112%',
          innerRadius: '70%',
          backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[count])
          .setOpacity(0.3)
          .get(),
          borderWidth: 0
        }]
      },

      yAxis: {
        min: 0,
        max: 100,
        lineWidth: 0,
        tickPositions: []
      },

      plotOptions: {
        solidgauge: {
          dataLabels: {
            enabled: false
          },
          linecap: 'round',
          stickyTracking: false,
          rounded: true
        }
      },

      series: [{
        name: chart_name,
        data: [{
          color: Highcharts.getOptions().colors[count],
          radius: '112%',
          innerRadius: '70%',
          y: percent_int
        }]
      },  ]
    });
  count++;
</script>

<?php 
$sum = 0;
$percent = 0;
}
?>
</div>

<H3 class="status-inprogress" style="padding-top: 100px"> Done </H3>
<div class="status-done" style="clear: both;">
  
  
</div>




<table align="center">
   <tr>
      <td width="800"><div id="container1" align="center"></div></td>
      <td ><div2><a href="withdraw.html"><button type="button" class="btn btn-outline-info add-button" style="margin-top: 0;">Withdraw</button></a></div></td>
   </tr>
</table>

<script type="text/javascript">

  Highcharts.setOptions({
    chart: {
      inverted: true,
      marginLeft: 135,
      type: 'bullet'
    },
    title: {
      text: null
    },
    legend: {
      enabled: false
    },
    yAxis: {
      gridLineWidth: 0
    },
    plotOptions: {
      series: {
        pointPadding: 0.25,
        borderWidth: 0,
        color: '#000',
        targetOptions: {
          width: '200%'
        }
      }
    },
    credits: {
      enabled: false
    },
    exporting: {
      enabled: false
    }
  });

  Highcharts.chart('container1', {
    chart: {
      marginTop: 40
    },
    title: {
      text: 'History Log'
    },
    xAxis: {
      categories: ['<span class="hc-cat-title"><?php echo($goals[$key]['name']); ?></span><br/> <?php echo($goals[$key]['price']); ?> บาท']
    },
    yAxis: {
      plotBands: [{ 
        from: 0,
        to: duration, // time band
        color: '#7A636D'
      }],
      title: null
    },
    series: [{
      data: [{
        y:duration-100, // goal
        target: 250 // real world
      }] 
    }],

    labels: {
      format: '{value}%'
    },
    tooltip: {
      pointFormat: '<b>{point.y}</b> (with target at {point.target})'
    }
  });


</script>






</body>
</html>
