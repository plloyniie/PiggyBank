<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Bootstrap 4 Login Form</title>

  <!-- Stylesheets -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="bootstrap4/css/bootstrap.min.css"/>
  <link rel="stylesheet" type="text/css" href="css/dashboard.css"/>
  <link href="web-fonts-with-css/css/fontawesome-all.css" rel="stylesheet">


  <!-- JS -->
  <script type="text/javascript" src="js/jquery.js"></script>   
</head>
<body>
  <?php
require('connect.php'); 
session_start();
$error='';
if (isset($_POST['submit'])) {
  if (empty($_POST['form-username']) || empty($_POST['form-password'])) {
    echo "Username or Password is invalid";
  } 
  else {

   $conn = db_connect();
   mysqli_set_charset($conn,"utf8");
   if (!$conn) {
     die("Connection failed: " . mysqli_connect_error());
   } else {

    $sql = "SELECT * FROM members WHERE username = '" .  $_POST['form-username'] . "' and password = '" . $_POST['form-password'] . "'";
    $result = mysqli_query($conn, $sql);
   
    if (mysqli_num_rows($result) > 0) {
      while ($row = $result->fetch_assoc()) {
        print_r($row);
        $_SESSION["uid"] = $row["uid"];
        $_SESSION["username"] = $row["username"];
        $_SESSION["kid"] = $row["kapook_id"];
        header("location: dashboard.php"); 
        session_write_close();


      }
    }else {
      $error = "Username or Password is incorrect!";
      console.log ('username and password incorrect!');
    }


    mysqli_close($conn);
   }
 }
} 

?>

  <div class="container">
    <form class="form-horizontal" role="form" method="POST" action="" >
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <h2>Please Login</h2>
          <hr>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <div class="form-group">
            <label class="sr-only" for="form-username">Username</label>
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-user"></i></div>
              <input type="text" name="form-username" class="form-control"
              placeholder="Username" required autofocus>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <div class="form-group">
            <label class="sr-only" for="form-password">Password</label>
            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
              <div class="input-group-addon" style="width: 2.6rem"><i class="fas fa-key"></i></div>
              <input type="password" name="form-password" class="form-control" id="password"
              placeholder="Password" required>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-control-feedback">
            <span class="text-danger align-middle">
              <!-- Put password error message here -->    
            </span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6" style="padding-top: .35rem">
          <div class="form-check mb-2 mr-sm-2 mb-sm-0">
            <label class="form-check-label">
              <input class="form-check-input" name="remember"
              type="checkbox" >
              <span style="padding-bottom: .15rem">Remember me</span>
            </label>
          </div>
        </div>
      </div>
      <div class="row" style="padding-top: 1rem">
        <div class="col-md-3"></div>
        <div class="col-md-6">
          <button name="submit" value= "login" type="submit" class="btn btn-success"><i class="fas fa-sign-in-alt"></i> Login</button>
          <!-- <button  class="btn btn-info"></button> -->
          
          <a class="btn btn-info" href="register.php" role="button"><i class="fa fa-user-plus"></i> Register</a>
          <a class="btn btn-link" href="/password/reset">Forgot Your Password?</a>
        </div>
      </div>
    </form>
  </div>
</body>
</html>